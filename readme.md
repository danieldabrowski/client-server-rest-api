#STARTER

``` bash
# 1.
composer install

# 2.
php artisan migrate

# 3
php artisan db:seed

# 4
npm install

# 5
npm run watch
```
