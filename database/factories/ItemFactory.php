<?php

use Faker\Generator as Faker;

$factory->define(App\Item::class, function (Faker $faker) {
    return [
        'name' => $faker->numerify('Produkt ##'),
        'amount' => $faker->randomDigit
    ];
});
