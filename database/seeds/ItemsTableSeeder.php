<?php

use Illuminate\Database\Seeder;

class ItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array('name'=>'Produkt 1', 'amount'=> 4),
            array('name'=>'Produkt 2', 'amount'=> 12),
            array('name'=>'Produkt 5', 'amount'=> 0),
            array('name'=>'Produkt 7', 'amount'=> 6),
            array('name'=>'Produkt 8', 'amount'=> 2),
        );
        App\Item::insert($data);
        // factory(App\Item::class, 20)->create();
    }
}
