<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Item;
use App\Http\Resources\ItemResource;

class ItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $q = ($request->input('q')) ?? '';
        $item = ($q == '0') ? Item::outStock()->get() : Item::inStock($q)->get();
        return ItemResource::collection($item);
    }

    /**
     * Store a created or edited resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $item = $request->isMethod('put') ? Item::findOrFail
        ($request->item_id) : new Item;

        $item->id = $request->input('item_id');
        $item->name = $request->input('name');
        $item->amount = $request->input('amount');

        if($item->save()) {
          return new ItemResource($item);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = Item::findOrFail($id);
        return new ItemResource($item);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Item::findOrFail($id);

        if($item->delete()) {
          return new ItemResource($item);
        }
    }
}
