<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
  /**
     * Scope a query items out of stock.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOutStock($query)
    {
        return $query->where('amount', '=', 0);
    }

    /**
     * Scope a query items depend on amount.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param mixed $amount
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeInStock($query, $amount = 0)
    {
        return $query->where('amount','>=',$amount);
    }
}
